import request from "@/utils/request";

const insertExam = (examDetail) => {
    return request({
        url: "/exam/insertExam",
        method: 'post',
        data:JSON.stringify(examDetail),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const queryExams = (examRequest) => {
    return request({
        url: "/exam/queryExams",
        method: 'post',
        data:JSON.stringify(examRequest),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const getMyExamList = (examListRequest) => {
    return request({
        url: "/exam/getMyExamList",
        method: 'post',
        data: JSON.stringify(examListRequest),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const getExam = (getExamRequest) => {
    return request({
        url: "/exam/getExam",
        method: 'post',
        data: JSON.stringify(getExamRequest),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const systemMarkExam = (systemExamRequest) => {
    return request({
        url: "/exam/systemMarkExam",
        method: 'post',
        data: JSON.stringify(systemExamRequest),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const copyExam = () => {
    return request({
        url: "/exam/copyExam",
        method: 'post'
    })
};
const changeExamStatus = (data) => {
    return request({
        url: "/exam/changeExamStatus",
        method: 'post',
        data: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const getMyExamResults = (data) => {
    return request({
        url: "/exam/getMyExamResults",
        method: 'post',
        data: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
export default {
    getMyExamResults,
    insertExam,
    queryExams,
    getMyExamList,
    getExam,
    systemMarkExam,
    copyExam,
    changeExamStatus
}