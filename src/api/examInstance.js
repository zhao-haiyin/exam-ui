import request from "@/utils/request";

const queryUserExamQuestionDetail = (resultCode) => {
    return request({
        url: "/exam-instance/queryUserExamQuestionDetail",
        method: 'post',
        data:JSON.stringify(resultCode),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
export default {
    queryUserExamQuestionDetail
}