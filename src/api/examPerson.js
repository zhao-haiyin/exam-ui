import request from "@/utils/request";

const getExamPersonCount = (examPersonCountRequest) => {
    return request({
        url: "/exam-person/getExamPersonCount",
        method: 'post',
        data: JSON.stringify(examPersonCountRequest),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const getExamPerson = (examPersonRequest) => {
    return request({
        url: "/exam-person/getExamPerson",
        method: 'post',
        data: JSON.stringify(examPersonRequest),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const exportExamPersonInfo = (exportRequest) => {
    return request({
        url: "/exam/exportExamPerson",
        method: 'post',
        data: JSON.stringify(exportRequest),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        },
        responseType: 'blob'
    })
};
export default {
    getExamPersonCount,
    getExamPerson,
    exportExamPersonInfo
}