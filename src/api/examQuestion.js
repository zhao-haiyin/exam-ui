import request from "@/utils/request";

const insertQuestion = (questionDetail) => {
    return request({
        url: "/exam-question/insertQuestion",
        method: 'post',
        data: JSON.stringify(questionDetail),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const queryQuestions = (questionSearch) => {
    return request({
        url: "/exam-question/queryQuestions",
        method: 'post',
        data: JSON.stringify(questionSearch),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
export default {
    insertQuestion,
    queryQuestions
}