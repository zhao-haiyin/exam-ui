import login from "./login"
import examQuestion from "./examQuestion"
import subject from "./subject"
import paper from "./paper"
import personBasis from "./personBasis"
import exam from "./exam"
import examPerson from "./examPerson"
import examInstance from "./examInstance"
export default{
    exam,
    login,
    examQuestion,
    subject,
    paper,
    personBasis,
    examPerson,
    examInstance
}