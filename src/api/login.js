import request from "../utils/request";
const login = (userInfo) => {
    return request({
        url: "/person/toLogin",
        method: 'post',
        data: userInfo
    })
};
const updatePwd = (userInfo) => {
    return request({
        url: "/person/updatePwd",
        method: 'post',
        data: JSON.stringify(userInfo),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
export default{
    login,
    updatePwd
}