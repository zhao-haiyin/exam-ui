import request from "../utils/request";
const queryPapers = (paperRequst) => {
    return request({
        url: "/exam-paper/queryPapers",
        method: 'post',
        data: JSON.stringify(paperRequst),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const insertPaper = (paperDetail) => {
    return request({
        url: "/exam-paper/insertExamPaper",
        method: 'post',
        data: JSON.stringify(paperDetail),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const queryPaperByExamType = (examType) => {
    return request({
        url: "/exam-paper/queryExamPaperByExamType",
        method: 'post',
        data: JSON.stringify(examType),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
export default{
    insertPaper,
    queryPaperByExamType,
    queryPapers
}