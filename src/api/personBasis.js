import request from "@/utils/request";

const departmentOrClass = (joinExamPersonType) => {
    return request({
        url: "/class/departmentOrClass",
        method: 'post',
        data: JSON.stringify(joinExamPersonType),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const queryPersons = (personsRequest) => {
    return request({
        url: "/exam-person/queryPersons",
        method: 'post',
        data: JSON.stringify(personsRequest),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const querySchoolTree = (schoolTreeRequest) => {
    return request({
        url: "/class/querySchoolTree",
        method: 'post',
        data: JSON.stringify(schoolTreeRequest),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const queryPersonsByCode = (personRequst) => {
    return request({
        url: "/person/queryPersonsByCode",
        method: 'post',
        data: JSON.stringify(personRequst),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const addDepartment = (departmentRequst) => {
    return request({
        url: "/class/addDepartment",
        method: 'post',
        data: JSON.stringify(departmentRequst),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const addClass = (classRequst) => {
    return request({
        url: "/class/addClass",
        method: 'post',
        data: JSON.stringify(classRequst),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const romoveSchoolByLayer = (removeSchoolRequst) => {
    return request({
        url: "/class/romoveSchoolByLayer",
        method: 'post',
        data: JSON.stringify(removeSchoolRequst),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const changeRole = (roleInfo) => {
    return request({
        url: "/role/changeRole",
        method: 'post',
        data: JSON.stringify(roleInfo),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const removeUser = (personId) => {
    return request({
        url: "/person/removeUser",
        method: 'post',
        data: JSON.stringify(personId),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
export default {
    removeUser,
    changeRole,
    departmentOrClass,
    queryPersons,
    querySchoolTree,
    queryPersonsByCode,
    addDepartment,
    addClass,
    romoveSchoolByLayer
}