import request from "@/utils/request";

const querySubject = () => {
    return request({
        url: "/exam-subject/querySubjects",
        method: 'post',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const querySubjectChild = (examType) => {
    return request({
        url: "/exam-subject/querySubjectChild",
        method: 'post',
        data: JSON.stringify(examType),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const querySubjectAllTree = () => {
    return request({
        url: "/exam-subject/querySubjectAllTree",
        method: 'post',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
const addSubject = (subjectRequest) => {
    return request({
        url: "/exam-subject/addSubject",
        method: 'post',
        data: JSON.stringify(subjectRequest),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    })
};
export default {
    querySubject,
    querySubjectChild,
    querySubjectAllTree,
    addSubject
}