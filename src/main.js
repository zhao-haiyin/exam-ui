import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'
import api from './api'
import router from './router'
// 关闭vue生产提示
Vue.prototype.$api = api
Vue.config.productionTip = false
Vue.use(ElementUI);

new Vue({
  el: '#app',
  render: h => h(App),
  router
}).$mount('#app')

