import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      // 重定向，默认首页
      redirect: "/login",
    },
    {
      path: "/login",
      //  路由懒加载第二种方式：
      component: () => import("../view/login.vue"),
      meta: {
        title: "登陆页面",
      },
      //使用钩子函数对路由进行权限跳转
      beforeEach:(to, from, next) => {
        document.title = `${to.meta.title}`;
        const role = sessionStorage.getItem("sessionId");
        if (!role && to.path !== "/login") {
          router.push("./login");
        } else {
          next();
        }
      }
    },
    {
      path: "/home",
      //  路由懒加载第二种方式：
      name: "homePage",
      component: () => import("../view/home.vue"),
      children: [],
      meta: {
        title: "首页",
      },
    },
    {
      path: "/examList",
      name: "examList",
      component: () => import("../view/childView/examListPage.vue"),
      meta: { title: "我的考试列表" },
    },
    {
      path: "/systemExam",
      name: "systemExam",
      component: () => import("../view/childView/systemExam.vue"),
      meta: { title: "考试" },
    },
    {
      path: "/systemManagement",
      name: "systemManagement",
      component: () => import("../view/childView/systemManagement.vue"),
      meta: { title: "系统管理" },
    },
    //404
    {
      path: "*",
      name: "notFound",
      component: () => import("../view/NotFound.vue"),
    },
    // {
    //     path: '/eduardo',
    //     component: () => import('../view/home.vue'),
    //     meta: {
    //       title: '首页'
    //     },
    //     children:[
    //         {
    //             path: '/eduardo/child',
    //             name:'child',
    //             component: () => import('../view/child.vue'),
    //             meta: {
    //             title: '儿子'
    //             }
    //         }
    //     ],
    // },
  ],
});


export default router;
