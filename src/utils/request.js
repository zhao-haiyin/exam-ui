import router from "@/router";
import axios from "axios"
//创建axios实例（instance）
const request = axios.create({
  baseURL: "http://localhost:80/",
  //前半段url
  timeout: 5000
  //如果5s后未响应，就通知用户页面存在加载错误
})

//请求拦截器
request.interceptors.request.use(config => {
  //什么时候执行这个函数？-发送请求之前
  //cofig是什么？-记录了本次请求相关信息的一个对象
  //这个函数能用来做什么？-做一些请求前可进行的操作（如添加请求头、获取token）
  const token = localStorage.getItem('sessionId')
  console.log(token)
  if (token) {
    config.headers.sessionId = token;
  }else{
    router.push('./login')
  }
  return config;
}, err => {
  router.push('./login')
  return Promise.reject(err)
});

//响应拦截器
request.interceptors.response.use(res => {
  //什么时候执行这个函数？-在接收到后端服务器的相应之后，进入到组件内部的then方法之前执行这里的代码
  //res是什么？-记是axios封装好的一个响应对象
  //这个函数能用来做什么？-做一些统一的数据处理
  return res.data//return后的值被组件中的请求的then方法的res接收
}, err => {
  return Promise.reject(err)
});

//导出
export default request