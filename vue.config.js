module.exports={
  lintOnSave:false,//关闭语法检查
  devServer:{
      proxy:{
          '/api':{
              target:'http://localhost/80',//你要跨域的服务器
              secure:false,
              changeOrigin: true, // 允许跨域
              pathRewrite:{'^/api':''},//位置替换，把请求的/api换为空   
          }
      }
  }
}
